import React, { Component } from 'react';
import { View, Text } from 'react-native';

import Home from './src/Components/Home'
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <View style={{ flex:1,justifyContent:'center' }}>
        <Home/>
      </View>
    );
  }
}

export default App;
